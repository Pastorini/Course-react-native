import React from "React";
import { View, Text } from "react-native";
import axios from 'axios';

class AlbumList extends React.Component{
    
    componentWillMount(){
        axios.get('https://rallycoding.herokuapp.com/api/music_albums')
            .then(response => console.log(response));
    }

    constructor(props){
        super(props);
    }

    render(){
        return (
            <View>
                <Text> Hello </Text>
            </View>
        );
    }
}

export default AlbumList;
