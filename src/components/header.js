// Import libraries for making a component
import React from "react";
import { Text, View } from "react-native";
import { BoxShadow } from "react-native-shadow";
import {Dimensions} from 'react-native';
var {height, width} = Dimensions.get('window');

// Make a component
const Header = (props) => {
  const { textStyle, viewStyle, shadowOpt } = styles;
  var t = props.headerText;
  return (
    <BoxShadow setting={shadowOpt}>
      <View style={viewStyle}>
        <Text style={textStyle}> {props.headerText} </Text>
      </View>
    </BoxShadow>
  );
};

const styles = {
  shadowOpt: {
    color: "#000",
    radius: 3,
    width: width,
    height: 62,
    opacity: 0.2
  },
  viewStyle: {
    backgroundColor: "#F0F8FF",
    justifyContent: "center",
    alignItems: "center",
    height: 60,
    paddingTop: 15,
    elevation: 2,
    position: 'relative'
  },
  textStyle: {
    fontSize: 35
  }
};

// Make the component available to other parts of the app
export default Header;
